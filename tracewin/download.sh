#!/bin/sh

# To set $pkgver:
source ./PKGBUILD
echo "Downloading version $pkgver .."

# Replace with hard-coded IP..
my_ip=`hostname -i | awk '{print $1}'`

download() {
    extension="${1##*.}"
    filename="${1%.*}"
    [ "$filename" = "$extension" ] && extension="" || extension=".$extension"
    outfilename="${filename}_${pkgver}$extension"
    [[ ! -f $outfilename ]] && wget "https://www.dacm-logiciels.fr/data/ForDownload/@$my_ip/$1" -O $outfilename
    file $outfilename | grep "HTML document"
    if (( $? == 0 ))
    then
        echo "Failed to download $1, ensure correctly initiated download links from https://dacm-logiciels.fr/downloads"
        rm $outfilename
    fi
}

download TraceWin
download TraceWin_noX11
download tracewin.pdf
echo ".. Done"
