#!/usr/bin/env python

import sys
import os
import argparse

binary = "/usr/share/toutatis/Toutatis-console"


parser = argparse.ArgumentParser(description="Toutatis RFQ Simulation code")

parser.add_argument("-i", dest="input", help="Input file (.INP)", required=True)

args = parser.parse_args()

if args.input != args.input.upper():
    raise ValueError("Input file must be all capital letters")

if not os.path.exists(args.input):
    raise ValueError(f"Could not find input file {args.input}")


os.system(f"{binary} -i {args.input}")
