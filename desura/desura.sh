#!/bin/bash
export LD_LIBRARY_PATH=./lib:$LD_LIBRARY_PATH

if [[ ! -f "${HOME}/desura/desura" ]]; then
    $HOME/desura/bin/desura
else
    ${HOME}/desura/desura
fi
